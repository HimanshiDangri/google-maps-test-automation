package stepDefintions;

import base.BaseUtil;
import common.TestBase;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks extends BaseUtil{
	
	private BaseUtil base;
	
	public Hooks(BaseUtil base) {
		this.base = base;
	}
	
	@Before
	public void setUp() {
		TestBase testBase = new TestBase();
		base.driver = testBase.initDriver(TestBase.prop.getProperty("browser"));
		testBase.initialPageSetUp(TestBase.prop.getProperty("URL"),Integer.parseInt(TestBase.prop.getProperty("ImplicitWait")));
	}
	

	@After
	public void tearDown() {
		base.driver.quit();
	}

}
