package stepDefintions;

import base.BaseUtil;
import common.Common;
import common.TestBase;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.DirectionsPage;
import pages.SearchPage;

public class StepDefinitions {
	Common common;
	SearchPage searchPage;
	DirectionsPage directionsPage;
	
	public StepDefinitions(BaseUtil base) {
		common = new Common(base.driver);
		searchPage = new SearchPage(base.driver);
		directionsPage = new DirectionsPage(base.driver);
	}
	
	@Given("User is on Google Maps Page")
	public void user_is_on_google_maps_page() {
		common.VerifyPageTitle(TestBase.prop.getProperty("Title"));
	}

	@When("User searches for {string}")
	public void user_searches_for(String string) {
		common.EnterSearchText(string);

	}

	@Then("Coordinates should be {string}")
	public void coordinates_should_be(String string) {
		searchPage.verifyCoordinates(string);
	}

	@When("User searches for driving directionss by Car option from {string} to {string}")
	public void user_searches_for_driving_directionss_by_car_option_from_to(String string, String string2) {
		common.EnterSearchText(string2);
		searchPage.ClickDirectionsIcon();
		directionsPage.enterStartingPoint(string);
		directionsPage.ClickCarRoute();
	}

	@Then("User should be able to view {string} or more routes should be displayed")
	public void user_should_be_able_to_view_or_more_routes_should_be_displayed(String string) {
		directionsPage.verifyRoutes(Long.parseLong(string));
		String filepath = System.getProperty("user.dir") + "/RoutesData/" + TestBase.prop.getProperty("RouteFileName");
		directionsPage.getRoutesDetailTextFile(filepath);
	}
	
}
