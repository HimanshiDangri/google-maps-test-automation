package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class NotepadReadWrite {

	public File openNotepad(String filepath) throws IOException {
		File file = new File(filepath);
		return file;
	}
	
	public void readNotepad(File file, String fileloc, String filename) throws IOException {
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferreader = new BufferedReader(fileReader);
		String filedata = "";
		while((filedata = bufferreader.readLine())!=null) {
			System.out.println(filedata + "\n");
		}
		bufferreader.close();
	}
	
	public void writenotepad(File file, String datatobewritten) throws IOException {
		if(!file.exists()) {
			file.createNewFile();

		}
		FileWriter filewriter = new FileWriter(file, true);
		BufferedWriter bw = new BufferedWriter(filewriter);
		bw.write(datatobewritten);
		bw.newLine();
		bw.close();
	}
	
	public void clearData(File file) throws IOException {
		if(file.exists()) {
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(filewriter);
			bw.write("");
			bw.close();
		}
	}

}

