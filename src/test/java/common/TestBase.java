package common;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class TestBase {

	private static WebDriver driver;
	public static final String projectPath = System.getProperty("user.dir");
	public static final Properties prop = new Properties();
	private static final Logger log = Logger.getLogger(TestBase.class);

	public TestBase() {
		try {
			FileInputStream ip = new FileInputStream(projectPath + "/src/test/java/resources/AppsSetting.properties");
			prop.load(ip);
		} catch (Exception e) {
			log.info(e.fillInStackTrace());
		}

	}


	public WebDriver initDriver(String browserName) {
		String browser = browserName.toUpperCase();
			switch (browser) {
			case "CHROME":{
				System.setProperty("webdriver.chrome.driver", projectPath + "/src/test/java/drivers/chromedriver.exe");
				driver = new ChromeDriver();
				break;
			}
			case "IE11":
				System.setProperty("webdriver.ie.driver", projectPath + "/src/test/java/drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				break;
			case "FIREFOX":
				System.setProperty("webdriver.gecko.driver", projectPath + "/src/test/java/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
				break;
			}
		return driver;

	}
	
	public void initialPageSetUp(String url, int waitTime) {
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
	}
	

}
