package common;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;



public class Common {
	
	private WebDriver driver;
	
    public Common(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(how = How.ID, using = "searchboxinput") 
    private WebElement txtSearchGoogleMaps;
    
    public void EnterSearchText(String text) {
    	txtSearchGoogleMaps.sendKeys(text);
    	txtSearchGoogleMaps.sendKeys(Keys.ENTER);
    }
    
    public void VerifyPageTitle(String expected) {
    	String actual = driver.getTitle();
    	Assert.assertEquals(expected, actual);
    }
    
    

}
