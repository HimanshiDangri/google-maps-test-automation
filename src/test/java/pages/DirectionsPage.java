package pages;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utils.NotepadReadWrite;

public class DirectionsPage {
	private static final Logger log = Logger.getLogger(DirectionsPage.class);
	
	public DirectionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

    @FindBy(how = How.XPATH, using = "//div[@id='sb_ifc51']/input") 
    private WebElement txtStartingPoint;
    
    @FindBy(how = How.XPATH, using = "//img[@aria-label='Driving']/parent::button")
    private WebElement iconCarRoute;
    
    @FindBy(how = How.XPATH, using = "//img[contains(@src,'drive_black.png')]") 
    private List<WebElement> iconCar;
    
    @FindBy(how = How.XPATH, using = "//h1[@class='section-directions-trip-title']")
    private List<WebElement> titleRoutes;
  
    @FindBy(how = How.XPATH, using = "//div[@class='section-directions-trip-duration delay-light']/child::span[1]")
    private List<WebElement> timeRoutes;
    
    @FindBy(how = How.XPATH, using = "//div[@class='section-directions-trip-distance section-directions-trip-secondary-text']/div")
    private List<WebElement> distanceRoutes;
    
    NotepadReadWrite notepadReadWrite = new NotepadReadWrite();
    
    public void ClickCarRoute() {
    	iconCarRoute.click();
    }
  
    public void enterStartingPoint(String text) {
    	txtStartingPoint.sendKeys(text);
    	txtStartingPoint.sendKeys(Keys.ENTER);
    }
     
    public void verifyRoutes(long expectedRoutesCount) {
    	long actualRoutesCount = iconCar.stream().count();
    	if(actualRoutesCount>=expectedRoutesCount)
    		Assert.assertTrue(true);
    }
    
    public void getRoutesDetailTextFile(String filepath) {
    	List<String> routesTitle = titleRoutes.stream().map(e->e.getText()).collect(Collectors.toList());
    	List<String> routesTime = timeRoutes.stream().map(e->e.getText()).collect(Collectors.toList());
    	List<String> routesDistance = distanceRoutes.stream().map(e->e.getText()).collect(Collectors.toList());
    	String routesData = "Route Title :: ";
    	for(int i=0; i<routesTitle.size(); i++) {
    		routesData += routesTitle.get(i) + "\r\n";
    		routesData += "Route Distance :: " + routesDistance.get(i) + "\r\n";
    		routesData += "Route Time :: " + routesTime.get(i) + "\r\n\r\n";
    	}
    	
		try {
			notepadReadWrite.clearData(notepadReadWrite.openNotepad(filepath));
			notepadReadWrite.writenotepad(notepadReadWrite.openNotepad(filepath), routesData);
		} catch (IOException e) {
			log.info(e.fillInStackTrace());
		}
    }
}
