package pages;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import common.TestBase;


public class SearchPage extends TestBase{

	private static final Logger log = Logger.getLogger(SearchPage.class);

	private WebDriver driver;

	public SearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

    @FindBy(how = How.XPATH, using = "//img[@alt='Directions']") 
    private WebElement iconDirections;
    
    public void ClickDirectionsIcon() {
    	iconDirections.click();
    }
    
	public void verifyCoordinates(String expected) {
		String actual = getCoordinates();
		log.info("actual value is " + actual);
		log.info("expected value is " + expected);
		Assert.assertEquals(expected, actual);
	}

	private String getCoordinates() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String currentURL = driver.getCurrentUrl();
		String[] urlInfo = currentURL.split("/");
		String coordinates = null;
		for (String s : urlInfo) {
			if (s.startsWith("@")) {
				coordinates = s;
				break;
			}
		}
		int index = coordinates.lastIndexOf(",");
		String finalCoordinates = coordinates.substring(1,index);
		return finalCoordinates;
	}
}
