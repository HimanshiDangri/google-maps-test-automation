Feature: Search and Directions Functionalities of Google Maps 
	This feature is to verify the search and directions functionalities of Google Maps
 
Scenario: Verify Search functionality of Google Maps 
	Given User is on Google Maps Page 
	When User searches for "San Fransisco, California" 
	Then Coordinates should be "37.757815,-122.5076407" 
	
Scenario: Verify Directions by Car functionality of Google Maps 
	Given User is on Google Maps Page 
	When User searches for driving directionss by Car option from "Chico, California" to "San Fransisco, California" 
	Then User should be able to view "2" or more routes should be displayed